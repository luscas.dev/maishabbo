import Head from 'next/head'
import styles from '../styles/Index.module.css'

export default function Home() {
  return (
    <div class={styles.app}>
      <Head>
        <title>Mais Habbo</title>
      </Head>

      <div>
        <div className={styles.logo}>
            <div className={styles.mais}></div>
            <div className={styles.mais}></div>
            <div className={styles.mais}></div>
        </div>

        <div className={styles.box}>
            <div className={styles.title}>
                <i>+</i>notícias
            </div>

            <div>
                {[...Array(5)].map(_ => (
                  <div className={styles.article}>
                    <div className={styles.image}></div>
                    <div className={styles.information}>
                        <div className={styles.title}>Uma nova equipe com nomes pesadíssimos, componham o novo conteúdo. Confira!</div>
                        <div className={styles.description}>Confira!</div>
                        <div className={styles.votes}>
                            <div className={styles.up}>
                                <img src="img/up.png" alt="Upvote" />
                                <div>8</div>
                            </div>
                            <div className={styles.up}>
                                <img src="img/down.png" alt="Downvote" />
                                <div>2</div>
                            </div>
                        </div>
                        <div className={styles.posted}>Postada em 25/12/2011 às 06:58. Por <b>MissFurby</b></div>
                    </div>
                </div>
              ))}
            </div>
        </div>
      </div>
    </div>
  )
}
